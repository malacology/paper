using Gtk 4.0;
using Adw 1;

template PaperNotebooksBar : Box {

  hexpand: false;
  vexpand: true;
  orientation: vertical;

  styles ["notebooks-bar"]

  Adw.HeaderBar header_bar {

    show-start-title-buttons: bind PaperNotebooksBar.paned;
    show-end-title-buttons: false;

    [title]
    Box {
      MenuButton {
        visible: bind PaperNotebooksBar.paned inverted;
        hexpand: false;
        vexpand: false;
        icon-name: "open-menu-symbolic";
        primary: true;
        popover: PopoverMenu primary_menu_popover {
          menu-model: primary_menu;
        };
      }

      Adw.WindowTitle window_title {
        visible: bind PaperNotebooksBar.paned;
      }
    }

    [end]
    MenuButton {
      visible: bind PaperNotebooksBar.paned;
      hexpand: false;
      vexpand: false;
      icon-name: "open-menu-symbolic";
      primary: true;
      popover: PopoverMenu primary_menu_popover_paned {
        menu-model: primary_menu;
      };
    }
  }

  ScrolledWindow scrolled_window {

    hexpand: false;
    vexpand: true;
    hscrollbar-policy: never;
    vscrollbar-policy: external;

    Box {
      orientation: vertical;

      Revealer all_button_revealer {
        reveal-child: false;
        transition-type: slide_down;

        ToggleButton all_button {

          visible: bind PaperNotebooksBar.paned inverted;
          icon-name: "view-list-symbolic";
          hexpand: false;
          vexpand: false;

          styles ["flat", "all-button"]
        }

        ToggleButton {

          visible: bind PaperNotebooksBar.paned;
          hexpand: false;
          vexpand: false;
          active: bind all_button.active bidirectional;
          sensitive: bind all_button.sensitive bidirectional;

          styles ["flat", "all-button"]

          Box {

            hexpand: true;
            orientation: horizontal;

            Image {
              icon-name: "view-list-symbolic";
            }

            Label {
              ellipsize: end;
              halign: start;
              label: _("All Notes");
            }
          }
        }
      }

      ListView list {
        hexpand: false;
        vexpand: true;
      }
    }
  }

  ToggleButton trash_button {

    visible: bind PaperNotebooksBar.paned inverted;
    icon-name: "user-trash-symbolic";
    hexpand: false;
    vexpand: false;

    styles ["flat", "trash-button"]
  }

  ToggleButton {

    visible: bind PaperNotebooksBar.paned;
    hexpand: false;
    vexpand: false;
    active: bind trash_button.active bidirectional;
    sensitive: bind trash_button.sensitive bidirectional;

    styles ["flat", "trash-button"]

    Box {

      hexpand: true;
      orientation: horizontal;

      Image {
        icon-name: "user-trash-symbolic";
      }

      Label {
        ellipsize: end;
        halign: start;
        label: _("Trash");
      }
    }
  }
}

menu primary_menu {
  section {
    item {
      custom: "theme";
    }
  }
  section {
    item {
      label: _("_New Notebook");
      action: "app.new-notebook";
    }
    item {
      label: _("_Edit Notebook");
      action: "app.edit-notebook";
    }
  }
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }
    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }
    item {
      label: _("_About");
      action: "app.about";
    }
  }
}